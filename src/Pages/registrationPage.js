module.exports = {
    salutationRadioButton: 'id_gender1',
	firstNameField: 'customer_firstname',
	lastNameField: 'customer_lastname',
	emailFieldOnRegPage: 'email',
	pwdField: 'passwd',
	daysDropdown: "//*[@id='days']",
	monthsDropdown: "//*[@id='months']",
	yearsDropdown: "//*[@id='years']",
	addressField: 'address1',
	cityField: 'city',
	stateField: "//*[@id='id_state']",
	postcodeField: 'postcode',
	phoneField: 'phone_mobile',
	registerButton: 'submitAccount',
	errorMessages: "//div[@class='alert alert-danger']",
	welcomeMsgPanel: "//p[@class='info-account']"
};