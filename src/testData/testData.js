module.exports = {
	loginPageTitle: "Login - My Store",
	registerButtonLabel: "Register",
	applicationURL: "http://automationpractice.com/index.php?controller=authentication&back=my-account",
	
	//Test data for registration
	selectedDate: "4  ",
	selectedMonth: "January ",
	selectedYear: "1987  ",
	selectedState: "Alaska",
	validZipCode: "00000",
	validPhoneNumber: "0987654321",
	invalidPassword: "a",
	invalidPostCode: "invalidPostCode",
	invalidPhoneNo: "invalidPhoneNo",
	invalidEmail: "test",
	
	//Error messages on login page
	invalidErrorMsgOnLoginPage: "Invalid email address.",
	
	//Error messages on registration page
	totalMissingFields: "There are 8 errors",
	phonenumberFieldErrorMsg: "You must register at least one phone number.",
	lastnameFieldErrorMsg: "lastname is required.",
	firstnameFieldErrorMsg: "firstname is required.",
	passwordFieldErrorMsg: "passwd is required.",
	addressFieldErrorMsg: "address1 is required.",
	cityFieldErrorMsg: "city is required.",
	zipcodeFieldErrorMsg: "The Zip/Postal code you've entered is invalid. It must follow this format: 00000",
	countryFieldErrorMsg: "This country requires you to choose a State.",
	invalidEmailMsg: "email is invalid.",
	invalidPasswordMsg: "passwd is invalid",
	invalidPhoneMsg: "phone_mobile is invalid.",
	
	
	//My Account Welcome Message
	welcomeMsg: "Welcome to your account. Here you can manage all of your personal information and orders."
	
};