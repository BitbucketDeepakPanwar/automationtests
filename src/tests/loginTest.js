const chromeDriver = require("../drivers/chrome");
const faker = require('faker');
const {Select, By, until} = require('selenium-webdriver');
const {emailField, submitButton} = require('../Pages/loginPage');
const {salutationRadioButton, firstNameField, lastNameField, pwdField, daysDropdown, monthsDropdown, yearsDropdown, addressField, 
       cityField, stateField, postcodeField, phoneField,registerButton} = require('../Pages/registrationPage');
const {loginPageTitle,	registerButtonLabel, applicationURL} = require('../testData/testData');	
	   
const TIMEOUT = 30000;

describe("Aura Code Challenge - Create User Account Tests", () => {
  let driver;
  beforeAll(() => {
    driver = chromeDriver();
  });

  afterAll(async () => {
    await driver.quit();
  });

  test("it loads authentication page", async () => {
    await driver.get(
      applicationURL
    );
	driver.manage().window().maximize();
    const title = await driver.getTitle();
    expect(title).toBe(loginPageTitle);
  });
  
  test("it validate email address and navigate to register page", async () => {
     //Entering correct email address and proceeding to personal information page
	await driver.findElement(By.id(emailField)).sendKeys(faker.internet.email());
	
	//Click Create an account
	await driver.findElement(By.name(submitButton)).click();
	await driver.manage().setTimeouts( { implicit: TIMEOUT, pageLoad: TIMEOUT, script: TIMEOUT } )
	
	const verifyText = await driver.findElement(By.id(registerButton)).getText();
    expect(verifyText).toBe(registerButtonLabel);
	
  });
});
