const chromeDriver = require("../drivers/chrome");
const faker = require('faker');
const {Select, By, until} = require('selenium-webdriver');
const {emailField, submitButton} = require('../Pages/loginPage');
const {salutationRadioButton, firstNameField, lastNameField, pwdField, daysDropdown, monthsDropdown, yearsDropdown, addressField, 
       cityField, stateField, postcodeField, phoneField,registerButton, welcomeMsgPanel} = require('../Pages/registrationPage');	   
const {loginPageTitle,	registerButtonLabel, selectedDate, selectedMonth, selectedYear, selectedState, validZipCode, validPhoneNumber, 
	    welcomeMsg, applicationURL} = require('../testData/testData'); 
const TIMEOUT = 30000;

describe("Aura Code Challenge - Create User Account Tests", () => {
  let driver;
  beforeAll(() => {
    driver = chromeDriver();
  });

  afterAll(async () => {
    await driver.quit();
  });

  test("it loads authentication page", async () => {
    await driver.get(
      applicationURL
    );
	driver.manage().window().maximize();
    const title = await driver.getTitle();
    expect(title).toBe(loginPageTitle);
  });
  
  test("it validate email address and navigate to register page", async () => {
	  //loginpage.loginWithValidEmailAddress();
     //Entering correct email address and proceeding to personal information page
	await driver.findElement(By.id(emailField)).sendKeys(faker.internet.email());
	
	//Click Create an account
	await driver.findElement(By.name(submitButton)).click();
	await driver.manage().setTimeouts( { implicit: TIMEOUT, pageLoad: TIMEOUT, script: TIMEOUT } )
	
	const verifyText = await driver.findElement(By.id(registerButton)).getText();
    expect(verifyText).toBe(registerButtonLabel);
	
  });
  
   test("Filling details and registering the user", async () => {
    //Filling Personal information
	await driver.findElement(By.id(salutationRadioButton)).click();
	await driver.findElement(By.id(firstNameField)).sendKeys(faker.name.firstName());
	await driver.findElement(By.id(lastNameField)).sendKeys(faker.name.lastName());
	await driver.findElement(By.id(pwdField)).sendKeys(faker.internet.password());
	
	const day =	await driver.findElement(By.xpath(daysDropdown))
	await day.click();
	await day.sendKeys(selectedDate);
	
	const month = await driver.findElement(By.xpath(monthsDropdown))
	await month.click();
	await month.sendKeys(selectedMonth);
	
	const year = await driver.findElement(By.xpath(yearsDropdown))
	await year.click();
	await year.sendKeys(selectedYear);
	
	//Filling Address Details
	await driver.findElement(By.id(addressField)).sendKeys(faker.address.streetAddress());
	await driver.findElement(By.id(cityField)).sendKeys(faker.address.city());
	
	const state = await driver.findElement(By.xpath(stateField))
	await state.click();
	await state.sendKeys(selectedState);
	
	await driver.findElement(By.id(postcodeField)).sendKeys(validZipCode);
	await driver.findElement(By.id(phoneField)).sendKeys(validPhoneNumber);
	await driver.findElement(By.id(registerButton)).click();
  }); 
  
  test("Verifying we are in My Account", async () => {
	await driver.manage().setTimeouts( { implicit: TIMEOUT, pageLoad: TIMEOUT, script: TIMEOUT } )	
	
	const verifyWelcomeMsg = await driver.findElement(By.xpath(welcomeMsgPanel)).getText();
    expect(verifyWelcomeMsg).toBe(welcomeMsg);  
  });
});
